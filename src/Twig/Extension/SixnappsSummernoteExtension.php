<?php
	
	namespace Sixnapps\SummernoteBundle\Twig\Extension;
	
	/**
	 * Class SixnappsSummernoteExtension
	 *
	 * @package Sixnapps\SummernoteBundle\Twig\Extension
	 */
	class SixnappsSummernoteExtension extends \Twig_Extension
	{
		
		/**
		 * @var
		 */
		protected $parameters;
		
		/**
		 * @var \Twig_Environment
		 */
		protected $twig;
		
		
		/**
		 * SixnappsSummernoteExtension constructor.
		 *
		 * @param                   $parameters
		 * @param \Twig_Environment $twig
		 */
		public function __construct( $parameters, \Twig_Environment $twig )
		{
			$this->parameters = $parameters;
			$this->twig       = $twig;
		}
		
		
		/**
		 * @return array|\Twig_Function[]
		 */
		public function getFunctions()
		{
			return [
				new \Twig_SimpleFunction( 'summernote_init', [ $this, 'summernoteInit' ], [ 'is_safe' => [ 'html' ] ] ),
			];
		}
		
		
		/**
		 * @return string
		 * @throws \Twig_Error_Loader
		 * @throws \Twig_Error_Runtime
		 * @throws \Twig_Error_Syntax
		 */
		public function summernoteInit()
		{
			$template                         = $this->parameters[ 'init_template' ];
			$options                          = [];
			$options[ 'fontname' ]            = count( $this->parameters[ 'fontname' ] ) > 0 ? $this->prepareArrayParameter( 'fontname' ) : $this->getDefaultFontname();
			$options[ 'fontnocheck' ]         = count( $this->parameters[ 'fontnocheck' ] ) > 0 ? $this->prepareArrayParameter( 'fontnocheck' ) : NULL;
			$options[ 'language' ]            = isset( $this->parameters[ 'language' ] ) ? $this->parameters[ 'language' ] : NULL;
			$options[ 'plugins' ]             = isset( $this->parameters[ 'plugins' ] ) ? $this->parameters[ 'plugins' ] : NULL;
			$options[ 'selector' ]            = $this->parameters[ 'selector' ];
			$options[ 'width' ]               = $this->parameters[ 'width' ];
			$options[ 'height' ]              = $this->parameters[ 'height' ];
			$options[ 'include_jquery' ]      = $this->parameters[ 'include_jquery' ];
			$options[ 'include_bootstrap' ]   = $this->parameters[ 'include_bootstrap' ];
			$options[ 'include_fontawesome' ] = $this->parameters[ 'include_fontawesome' ];
			$options[ 'fontawesome_path' ]    = $this->parameters[ 'fontawesome_path' ];
			$options[ 'bootstrap_css_path' ]  = $this->parameters[ 'bootstrap_css_path' ];
			$options[ 'bootstrap_js_path' ]   = $this->parameters[ 'bootstrap_js_path' ];
			$options[ 'jquery_path' ]         = $this->parameters[ 'jquery_path' ];
			$options[ 'summernote_css_path' ] = $this->parameters[ 'summernote_css_path' ];
			$options[ 'summernote_js_path' ]  = $this->parameters[ 'summernote_js_path' ];
			$options[ 'jquery_version' ]      = $this->parameters[ 'jquery_version' ];
			$options[ 'toolbar' ]             = $this->prepareToolbar();
			$base_path                        = ( !isset( $this->parameters[ 'base_path' ] ) ? 'bundles/sixnappssummernote/' : $this->parameters[ 'base_path' ] );
			return $this->twig->render( $template, [ 'sn' => $options, 'base_path' => $base_path ] );
		}
		
		
		/**
		 * @return string
		 */
		private function prepareToolbar()
		{
			if ( empty( $this->parameters[ 'toolbar' ] ) && empty( $this->parameters[ 'extra_toolbar' ] ) ) {
				return '';
			}
			$str     = '[';
			$toolbar = $this->parameters[ 'toolbar' ];
			$str     .= $this->processToolbar( $toolbar );
			if ( !empty( $this->parameters[ 'extra_toolbar' ] ) ) {
				if ( empty( $this->parameters[ 'toolbar' ] ) ) {
					$str .= $this->getDefaultToolbar();
				}
				$str .= $this->processToolbar( $this->parameters[ 'extra_toolbar' ] );
			}
			$str .= ']';
			
			return $str;
		}
		
		
		/**
		 * @param $name
		 *
		 * @return string
		 */
		private function prepareArrayParameter( $name )
		{
			if ( isset( $this->parameters[ $name ] ) ) {
				$parameterArray = $this->parameters[ $name ];
				$count          = count( $parameterArray );
				$str            = "['" . $parameterArray[ 0 ] . "'";
				for ( $i = 1; $i < $count; ++$i ) {
					$str .= ", '" . $parameterArray[ $i ] . "'";
				}
				$str .= ']';
				return $str;
			}
			
			return NULL;
		}
		
		
		/**
		 * @param array $toolbar
		 *
		 * @return string
		 */
		private function processToolbar( array $toolbar )
		{
			$str = '';
			foreach ( $toolbar as $key => $tb ) {
				$str .= sprintf( "[ '%s', ", $key );
				$str .= json_encode( $tb );
				$str .= '], ';
			}
			return $str;
		}
		
		
		/**
		 * @return string
		 */
		private function getDefaultToolbar()
		{
			return "['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']],";
		}
		
		
		/**
		 * @return string
		 */
		public function getDefaultFontname()
		{
			return "['Arial', 'Courier New', 'Helvetica', 'Times New Roman']";
		}
		
		
		/**
		 * @return string
		 */
		public function getName()
		{
			return 'sixnapps_summernote';
		}
	}
