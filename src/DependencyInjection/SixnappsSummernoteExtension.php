<?php

	namespace Sixnapps\SummernoteBundle\DependencyInjection;
	
	use Symfony\Component\Config\FileLocator;
	use Symfony\Component\DependencyInjection\ContainerBuilder;
	use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
	use Symfony\Component\HttpKernel\DependencyInjection\Extension;
	
	/**
	 * Class SixnappsSummernoteExtension
	 *
	 * @package Sixnapps\SummernoteBundle\DependencyInjection
	 */
	class SixnappsSummernoteExtension extends Extension
	{
		/**
		 * @param array            $configs
		 * @param ContainerBuilder $container
		 *
		 * @throws \Exception
		 */
		public function load(array $configs, ContainerBuilder $container)
		{
			$config = $this->processConfiguration(new Configuration(), $configs);
			$loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
			$loader->load('services.xml');
			$container->setParameter('sixnapps_summernote', $config);
		}
		
		
		/**
		 * @return string
		 */
		public function getAlias()
		{
			return 'sixnapps_summernote';
		}
		
		
		/**
		 * @return string
		 */
		public function getNamespace()
		{
//			return 'http://helios-ag.github.io/schema/dic/fm_summernote';
			return 'sixnapps_summernote';
		}
	}
