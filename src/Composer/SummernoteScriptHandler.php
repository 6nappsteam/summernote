<?php
	namespace Sixnapps\SummernoteBundle;
	
	use Composer\Script\CommandEvent;
	use Composer\Script\Event;
	use Sixnapps\SummernoteBundle\Installer\SummernoteInstaller;
	use Sensio\Bundle\DistributionBundle\Composer\ScriptHandler;
	
	/**
	 * Class SummernoteScriptHandler
	 *
	 * @package Sixnapps\SummernoteBundle
	 */class SummernoteScriptHandler extends ScriptHandler
	{
		
		/**
		 * @param $event
		 */
		public static function install($event)
		{
			$installer = new SummernoteInstaller();
			$installer->install();
		}
	}
