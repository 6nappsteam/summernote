<?php
	
	namespace Sixnapps\SummernoteBundle;
	
	use Symfony\Component\DependencyInjection\ContainerBuilder;
	use Symfony\Component\HttpKernel\Bundle\Bundle;
	
	/**
	 * Class SixnappsSummernoteBundle
	 */
	class SixnappsSummernoteBundle extends Bundle
	{
		public function build( ContainerBuilder $container )
		{
			parent::build( $container );
			
		}
	}
