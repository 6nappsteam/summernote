<?php
	
	namespace Sixnapps\SummernoteBundle\Installer;
	
	use Symfony\Component\OptionsResolver\Options;
	use Symfony\Component\OptionsResolver\OptionsResolver;
	
	/**
	 * Class SummernoteInstaller
	 *
	 * @package Sixnapps\SummernoteBundle\Installer
	 */
	class SummernoteInstaller
	{
		const CLEAR_DROP               = 'drop';
		
		const CLEAR_KEEP               = 'keep';
		
		const CLEAR_SKIP               = 'skip';

		const NOTIFY_CLEAR             = 'clear';

		const NOTIFY_CLEAR_ARCHIVE     = 'clear-archive';

		const NOTIFY_CLEAR_COMPLETE    = 'clear-complete';

		const NOTIFY_CLEAR_PROGRESS    = 'clear-progress';

		const NOTIFY_CLEAR_QUESTION    = 'clear-question';

		const NOTIFY_CLEAR_SIZE        = 'clear-size';

		const NOTIFY_DOWNLOAD          = 'download';

		const NOTIFY_DOWNLOAD_COMPLETE = 'download-complete';

		const NOTIFY_DOWNLOAD_PROGRESS = 'download-progress';

		const NOTIFY_DOWNLOAD_SIZE    = 'download-size';

		const NOTIFY_EXTRACT          = 'extract';

		const NOTIFY_EXTRACT_COMPLETE = 'extract-complete';

		const NOTIFY_EXTRACT_PROGRESS = 'extract-progress';

		const NOTIFY_EXTRACT_SIZE = 'extract-size';
		/**
		 * @var string
		 */
		private static $archive = 'https://https://github.com/summernote/summernote/archive/%s.zip';
		/**
		 * @var OptionsResolver
		 */
		private $resolver;
		
		
		/**
		 * @param mixed[] $options
		 */
		public function __construct( array $options = [] )
		{
			$this->resolver = ( new OptionsResolver() )
				->setDefaults( array_merge( [
					'clear'    => NULL,
					'notifier' => NULL,
					'path'     => dirname( __DIR__ ) . '/Resources/public',
					'version'  => self::VERSION_LATEST,
				], $options ) )
				->setAllowedTypes( 'excludes', 'array' )
				->setAllowedTypes( 'notifier', [ 'null', 'callable' ] )
				->setAllowedTypes( 'path', 'string' )
				->setAllowedTypes( 'version', 'string' )
				->setAllowedValues( 'clear', [ self::CLEAR_DROP, self::CLEAR_KEEP, self::CLEAR_SKIP, NULL ] )
				->setNormalizer( 'path', function ( Options $options, $path ) {
					return rtrim( $path, '/' );
				} )
			;
		}
		
		
		/**
		 * @param array $options
		 *
		 * @return bool
		 */
		public function install( array $options = [] )
		{
			$options = $this->resolver->resolve( $options );
			if ( self::CLEAR_SKIP === $this->clear( $options ) ) {
				return FALSE;
			}
			$this->extract( $this->download( $options ), $options );
			return TRUE;
		}
		
		
		/**
		 * @param array $options
		 *
		 * @return string
		 */
		private function clear( array $options )
		{
			if ( !file_exists( $options[ 'path' ] . '/summernote.js' ) ) {
				return self::CLEAR_DROP;
			}
			if ( NULL === $options[ 'clear' ] && NULL !== $options[ 'notifier' ] ) {
				$options[ 'clear' ] = $this->notify( $options[ 'notifier' ], self::NOTIFY_CLEAR, $options[ 'path' ] );
			}
			if ( NULL === $options[ 'clear' ] ) {
				$options[ 'clear' ] = self::CLEAR_SKIP;
			}
			if ( self::CLEAR_DROP === $options[ 'clear' ] ) {
				$files = new \RecursiveIteratorIterator(
					new \RecursiveDirectoryIterator( $options[ 'path' ], \RecursiveDirectoryIterator::SKIP_DOTS ),
					\RecursiveIteratorIterator::CHILD_FIRST
				);
				$this->notify( $options[ 'notifier' ], self::NOTIFY_CLEAR_SIZE, iterator_count( $files ) );
				foreach ( $files as $file ) {
					$filePath = $file->getRealPath();
					$this->notify( $options[ 'notifier' ], self::NOTIFY_CLEAR_PROGRESS, $filePath );
					if ( $dir = $file->isDir() ) {
						$success = @rmdir( $filePath );
					} else {
						$success = @unlink( $filePath );
					}
					if ( !$success ) {
						throw $this->createException( sprintf(
							'Unable to remove the %s "%s".',
							$dir ? 'directory' : 'file',
							$filePath
						) );
					}
				}
				$this->notify( $options[ 'notifier' ], self::NOTIFY_CLEAR_COMPLETE );
			}
			return $options[ 'clear' ];
		}
		
		
		/**
		 * @param array $options
		 *
		 * @return bool|string
		 */
		private function download( array $options )
		{
			$url = sprintf( self::$archive, $options[ 'version' ] );
			$this->notify( $options[ 'notifier' ], self::NOTIFY_DOWNLOAD, $url );
			$zip = @file_get_contents( $url, FALSE, $this->createStreamContext( $options[ 'notifier' ] ) );
			if ( FALSE === $zip ) {
				throw $this->createException( sprintf( 'Unable to download Summernote ZIP archive from "%s".', $url ) );
			}
			$path = tempnam( sys_get_temp_dir(), 'summernote-' . $options[ 'version' ] . '.zip' );
			if ( !@file_put_contents( $path, $zip ) ) {
				throw $this->createException( sprintf( 'Unable to write Summernote ZIP archive to "%s".', $path ) );
			}
			$this->notify( $options[ 'notifier' ], self::NOTIFY_DOWNLOAD_COMPLETE, $path );
			return $path;
		}
		
		
		/**
		 * @param callable|NULL $notifier
		 *
		 * @return resource
		 */
		private function createStreamContext( callable $notifier = NULL )
		{
			$context = [];
			$proxy   = getenv( 'https_proxy' ) ? : getenv( 'http_proxy' );
			if ( $proxy ) {
				$context[ 'proxy' ]           = $proxy;
				$context[ 'request_fulluri' ] = (bool) getenv( 'https_proxy_request_fulluri' ) ? :
					getenv( 'http_proxy_request_fulluri' );
			}
			return stream_context_create( $context, [
				'notification' => function (
					$code,
					$severity,
					$message,
					$messageCode,
					$transferred,
					$size
				) use ( $notifier ) {
					if ( NULL === $notifier ) {
						return;
					}
					switch ( $code ) {
						case STREAM_NOTIFY_FILE_SIZE_IS:
							$this->notify( $notifier, self::NOTIFY_DOWNLOAD_SIZE, $size );
							break;
						case STREAM_NOTIFY_PROGRESS:
							$this->notify( $notifier, self::NOTIFY_DOWNLOAD_PROGRESS, $transferred );
							break;
					}
				},
			] );
		}
		
		
		/**
		 * @param       $path
		 * @param array $options
		 */
		private function extract( $path, array $options )
		{
			$this->notify( $options[ 'notifier' ], self::NOTIFY_EXTRACT, $options[ 'path' ] );
			$zip = new \ZipArchive();
			$zip->open( $path );
			$this->notify( $options[ 'notifier' ], self::NOTIFY_EXTRACT_SIZE, $zip->numFiles );
			$offset = 20 + strlen( $options[ 'release' ] ) + strlen( $options[ 'version' ] );
			for ( $i = 0; $i < $zip->numFiles; ++$i ) {
				$this->extractFile(
					$file = $zip->getNameIndex( $i ),
					substr( $file, $offset ),
					$path,
					$options
				);
			}
			$zip->close();
			$this->notify( $options[ 'notifier' ], self::NOTIFY_EXTRACT_COMPLETE );
			$this->notify( $options[ 'notifier' ], self::NOTIFY_CLEAR_ARCHIVE, $path );
			if ( !@unlink( $path ) ) {
				throw $this->createException( sprintf( 'Unable to remove the Summernote ZIP archive "%s".', $path ) );
			}
		}
		
		
		/**
		 * @param       $file
		 * @param       $rewrite
		 * @param       $origin
		 * @param array $options
		 */
		private function extractFile( $file, $rewrite, $origin, array $options )
		{
			$this->notify( $options[ 'notifier' ], self::NOTIFY_EXTRACT_PROGRESS, $rewrite );
			$from = 'zip://' . $origin . '#' . $file;
			$to   = $options[ 'path' ] . '/' . $rewrite;
			foreach ( $options[ 'excludes' ] as $exclude ) {
				if ( 0 === strpos( $rewrite, $exclude ) ) {
					return;
				}
			}
			if ( '/' === substr( $from, -1 ) ) {
				if ( !is_dir( $to ) && !@mkdir( $to ) ) {
					throw $this->createException( sprintf( 'Unable to create the directory "%s".', $to ) );
				}
				return;
			}
			if ( !@copy( $from, $to ) ) {
				throw $this->createException( sprintf( 'Unable to extract the file "%s" to "%s".', $file, $to ) );
			}
		}
		
		
		/**
		 * @param callable|NULL $notifier
		 * @param               $type
		 * @param null          $data
		 *
		 * @return mixed
		 */
		private function notify( callable $notifier = NULL, $type, $data = NULL )
		{
			if ( NULL !== $notifier ) {
				return $notifier( $type, $data );
			}
		}
		
		
		/**
		 * @param $message
		 *
		 * @return \RuntimeException
		 */
		private function createException( $message )
		{
			$error = error_get_last();
			if ( isset( $error[ 'message' ] ) ) {
				$message .= sprintf( ' (%s)', $error[ 'message' ] );
			}
			return new \RuntimeException( $message );
		}
	}
