(function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals: jQuery
        factory(window.jQuery);
    }
}(function ($){
    // template, editor
    var tmpl = $.summernote.renderer.getTemplate();
    // add plugin
    $.summernote.addPlugin({
        name: 'ElFinder', // name of plugin
        buttons: { // buttons
            elfinder: function (context) {
                return tmpl.iconButton('fa fa-picture-o', {
                    event: 'elfinder',
                    title: 'Galerie / fichiers',
                    hide: false
                });
            }
        },

        events: { // events
            elfinder: function (event, editor, layoutInfo) {
                elFinderBrowser();
            }

        }
    });
}));

function elfinderDialog(context){
    var fm = $('<div/>').dialogelfinder({
        title: "Galerie photos",
        url : '/templates/altair/dist/file_manager/php/connector.custom.php',
        lang : 'fr',
        width : $(window).width() * .8,
        height: $(window).height() * .8,
        destroyOnClose : true,
        getFileCallback : function(file, fm) {
            var url = file.url.replace('uploads/galerie', 'static')
            context.invoke('editor.insertImage', fm.convAbsUrl(url)); 
        },
        commandsOptions : {
            getfile : {
                oncomplete : 'close',
                folders : false
            }
        }
    }).dialogelfinder('instance');
}

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('jquery'));
    } else {
        factory(window.jQuery);
    }
}(function ($) {
    $.extend($.summernote.plugins, {
        'elfinder': function (context) {
            var self = this;
            var ui = $.summernote.ui;
            context.memo('button.elfinder', function () {
                var button = ui.button({
                    contents: '<i class="material-icons">insert_photo</i>',
                    tooltip: 'Galerie photos',
                    click: function () {
                        elfinderDialog(context);
                    }
                });

                var $elfinder = button.render();
                return $elfinder;
            });

            this.destroy = function () {
                this.$panel.remove();
                this.$panel = null;
            };
        }
    });
}));
